## Contributing

1. Fork the repository.
2. Create a separate branch for your change.
3. Add relevant tests for your changes. Note that acceptance changes must be in place for new or modified puppet types.
4. Run the tests! Do this locally on your machine as only passing tests will be accepted!
5. Squash your commits down into logical components. 
6. Be sure to rebase against the current master before committing.
7. Push your branch to your fork and submit a pull request.

## Dependencies

* PDK
* Beaker
* Vagrant

## Running Tests

All of the following tests will need to pass before your changes will be accepted.

### PDK Syntax

Ensure your code passes basic validation.

```bash
pdk validate
```

Note that if you've run the acceptance tests already at you'll need to remove the `.vagrant` directory to get around pdk validate crashing. As of PDK 1.9 there is no way to prevent the yaml test from finding the './.vagrant/beaker_vagrant_files/centos-7-64.yml' directory and failing to validate it.

```
VAGRANT_DIR="./.vagrant"
if [ -d "${VAGRANT_DIR}" ]
then
  rm -rf "${VAGRANT_DIR}"
fi
```

### PDK Unit Tests

Execute the standard unit tests.

```bash
pdk test unit
```

### Acceptance Tests (Beaker)

Run acceptance tests for each valid version of puppet.

```bash
BEAKER_PUPPET_COLLECTION=puppet5 pdk bundle exec rake beaker:centos-7-64
BEAKER_PUPPET_COLLECTION=puppet6 pdk bundle exec rake beaker:centos-7-64
```

If you run into issues testing it can be helpful to have vagrant leave the VM on test completion. To perform this simply set the variable `BEAKER_destroy=no` before the call to beaker. For example,

```bash
BEAKER_destroy=no BEAKER_PUPPET_COLLECTION=puppet5 pdk bundle exec rake beaker:centos-7-64
```
