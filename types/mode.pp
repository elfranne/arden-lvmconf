type Lvmconf::Mode = Enum[
  'halvm',
  'cluster',
  'standard',
]
