# @summary Enforces lvm.conf based on lvmconf mode flags
class lvmconf::config {
  # Ensure our whitelist is populated appropriately
  if ! empty($lvmconf::local_vgs) {
    $volume_string = join($lvmconf::local_vgs, '","')
    lvm_config { 'activation/volume_list':
      value => "[\"${volume_string}\"]"
    }
  } elsif $lvmconf::mode != 'standard' {
    fail("lvmconf::config: A volume list must be specified when mode ${lvmconf::mode} is enabled!")
  }
  # TODO: handle clearing the volume list

  # Configure locking mode
  case $lvmconf::mode {
    'standard','halvm': {
      lvm_config { 'global/locking_type':
        value => '1',
      }
    }
    'cluster': {
      lvm_config { 'global/locking_type':
        value => '3',
      }
    }
    default: {}
  }

  # Configure lvmetad
  case $lvmconf::mode {
    'standard': {
      lvm_config { 'global/use_lvmetad':
        value => '1',
      }
    }
    'halvm', 'cluster': {
      lvm_config { 'global/use_lvmetad':
        value => '0',
      }
    }
    default: {}
  }
}
