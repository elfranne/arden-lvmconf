# lvmconf

[![pipeline status](https://gitlab.com/arden-puppet/arden-lvmconf/badges/master/pipeline.svg)](https://gitlab.com/arden-puppet/arden-lvmconf/commits/master) [![Version](https://img.shields.io/puppetforge/v/arden/lvmconf.svg)](https://forge.puppet.com/arden/lvmconf) [![coverage report](https://gitlab.com/arden-puppet/arden-lvmconf/badges/master/coverage.svg)](https://gitlab.com/arden-puppet/arden-lvmconf/commits/master)

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with lvmconf](#setup)
    * [What lvmconf affects](#what-lvmconf-affects)
    * [Beginning with lvmconf](#beginning-with-lvmconf)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Limitations - OS compatibility, etc.](#limitations)
5. [Development - Guide for contributing to the module](#development)

## Description

The lvmconf module delivers two main sets of functionality:
1. A resource type for directly managing entries in `lvm.conf` called `lvm_config`.
2. Pre-built logic to emulate the behavior of the `lvmconf` used to configure a system for HA LVM.

## Setup

### What lvmconf affects

#### 1 - Packages

When enabled lvmconf will attempt to install the lvm2 package. Note that hiera defaults exist for RedHat 7 family operating systems, however, other operating systems will need to have the `package_name` parameter specified.

#### 2 - lvm config

This class includes a provider for manipulating entries in `/etc/lvm/lvm.conf` using the `lvmconfig` command. Depending on the mode selected in the main class several configuration directives will be updated automatically. Currently this is limited to the following:

* `global/use_lvmetad` - Controls the use of the LVM metadata daemon. When HA LVM is configured this must be disabled
* `global/locking_type` - Controls the locking method used by LVM. This is needs to be set to standard locking for both halvm and standard modes of operation.

When `halvm` is enabled it is also important to configure the `activation/volume_list` parameter. This provides a white-list of volume groups which are safe for LVM to automatically import. Obviously any volumes which are part of an HA LVM scenario should be excluded from this list.

#### 2 - Services

The state of the following services is managed automatically when `mode` is changed:

* `metad` - the lvm metadata daemon (lvmetad). In a standard system without HA LVM this daemon is used to accelerate lvm query commands by caching states in memory. This must be disabled when used with HA LVM

Additionally, if enabled, this class can automatically regenerate the initramfs to incorporate the changes to `lvm.conf`.

### Beginning with lvmconf

On a RedHat 7 based operating system enabling standard configuration can be performed via the following simple call:

```puppet
class { 'lvmconf':
  manage_package => true,
  mode           => 'standard',
}
```

## Usage

The following examples assume a RedHat 7 based operating system. If a different operating system is used which does not have module hiera data additional parameters will need to be specified.

### Enabling HA LVM

```puppet
class { 'lvmconf':
  mode             => 'halvm'
  manage_initramfs => true,
  local_vgs        => [ 'rootvg' ],
}
```

Alternatively the same configuration can be performed via hiera.

```yaml
lvmconf::mode: 'halvm'
lvmconf::manage_initramfs: true
lvmconf::local_vgs:
  - 'rootvg'
```

```puppet
include lvmconf
```

### Enabling Standard LVM

```puppet
class { 'lvmconf':
  mode             => 'standard'
  manage_initramfs => true,
}
```

Alternatively the same configuration can be performed via hiera.

```yaml
lvmconf::mode: 'standard'
lvmconf::manage_initramfs: true
```

```puppet
include lvmconf
```

## Limitations

* Enabling / disabling `clvmd` or `cmirrord` is currently not supported by this module.
* Initramfs updates are limited to dracut, however, other generators such as genkernel likely could be added.
* When `lvm.conf` parameters are modified there is no way to automatically reset them to default values. Such changes must be carried out directly on the target nodes.

## Development

See the [contributing guide](https://gitlab.com/arden-puppet/arden-lvmconf/blob/master/CONTRIBUTING.md).

## Contributors

Check out the [contributor list](https://gitlab.com/arden-puppet/arden-lvmconf/graphs/master).
