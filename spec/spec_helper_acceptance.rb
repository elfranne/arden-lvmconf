# frozen_string_literal: true

# This file comes from puppetlabs-stdlib
# which is licensed under the Apache-2.0 License.
# https://github.com/puppetlabs/puppetlabs-stdlib
# (c) 2019-2019 Puppetlabs and puppetlabs-stdlib contributors

require 'beaker-pe'
require 'beaker-puppet'
require 'puppet'
require 'beaker-rspec'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper unless ENV['BEAKER_provision'] == 'no'
configure_type_defaults_on(hosts)
install_ca_certs unless %r{pe}i.match?(ENV['PUPPET_INSTALL_TYPE'])
install_module_on(hosts)
install_module_dependencies_on(hosts)

RSpec.configure do |c|
  # Readable test descriptions
  c.formatter = :documentation

  # Configure all nodes in nodeset
  c.before :suite do
  end
end

def return_puppet_version
  (on default, puppet('--version')).output.chomp
end
